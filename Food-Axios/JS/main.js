const BASE_URL="https://63b2c9a15e490925c521a619.mockapi.io";
function renderFoodList(foods){
  var contentHTML="";
  foods.reverse().  forEach(function(item){
    var content =`<tr>
    <td>${item.maMon}</td>
    <td>${item.tenMon}</td>
    <td>${item.giaMon}</td>
    <td>${item.loaiMon ?"<span class='text-success'>Mặn</span>":
    "<span class='text-primary'>Chay</span>" }</td>
    <td>${converseString(30,item.hinhAnh)}</td>
    <td>
    <button onclick="xoaMonAn('${item.maMon}')" class="btn btn-danger">Xóa</button>
    <button onclick="suaMonAn('${item.maMon}')" class="btn btn-warning">Sửa</button>
    </td>
    </tr>`
    contentHTML+=content;
  })
  document.getElementById("tbodyFood").innerHTML=contentHTML;
};

function fectchFoodList(){
  batLoading();
  axios({
    url:`${BASE_URL}/food`,
    method:"GET",
  }).then(function(res){
    tatLoading()
    var foodList = res.data;
    renderFoodList(foodList);
  }).catch(function(err){
    tatLoading()
    console.log(err);
  })
}
fectchFoodList();

function xoaMonAn(id){
  batLoading()
axios({
  url:`${BASE_URL}/food/${id}`,
  method:"DELETE",
}).then(function(res){
  tatLoading()
fectchFoodList();
}).catch(function(err){
  tatLoading()
  console.log(err);
})
}

function themMonAn(){
  batLoading();
  var monAn = layThongTinTuForm();
  axios({
    url:`${BASE_URL}/food`,
    method:"POST",
    data:monAn,
  }).then(function(res){
    tatLoading()
    fectchFoodList();
  }).catch(function(err){
    tatLoading()
    console.log(err);
  })
}
function suaMonAn(id){
  batLoading()
  batCpaNhap();
axios({
  url:`${BASE_URL}/food/${id}`,
  method:"GET",
}).then(function(res){
  tatLoading()
  document.getElementById("maMon").disabled=true;
  var foodItem = res.data;
  showThongTinLenForm(foodItem);
}).catch(function(err){
  tatLoading()
  console.log(err);
})
}
function capNhapMonAn(){
  var monAn = layThongTinTuForm();
  axios({
    url:`${BASE_URL}/food/${monAn.maMon}`,
    method:"PUT",
    data:monAn,
  }).then(function(res){
  fectchFoodList()
  }).catch(function(err){
    console.log(err);
  })
}